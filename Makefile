# Makefile
#
# Network Systems root CA (for DNAC and possibly other) certificates
#
# This make file builds the hierarchy of private certificates.


.PHONY: all
all: dnac.crt root.crt

.PHONY: clean
clean:
	-rm dnac.crt root.crt root.csr root.key

.PHONY: clean-example
clean-example:
	-rm dnac.csr dnac-example.key

.PHONY: remake
remake: clean all


# the private CA-signed certificate and key
#
# dnac.csr comes from DNAC itself - we don't generate it here
#
# for test purposes only, however, we can generate one by making dnac.csr
#
# copy_extensions requires OpenSSL v3.0.1 or later, or the extensions will
# need to be manually re-applied when signing

dnac.crt: dnac.csr root.crt
	@echo MAKE DNAC CRT
	openssl-3 x509 -req -days 1837 -sha256 -CAcreateserial -CA root.crt \
	  -CAkey root.key -in dnac.csr -out dnac.crt -copy_extensions copy

# deliberately build a different file so it won't be triggered except
# explicitly

.PHONY: dnac-exmaple.csr
dnac-example.csr: dnac-example.key dnac-example.cnf
	@echo MAKE DNAC CSR
	openssl req -new -config dnac-example.cnf -key dnac-example.key \
	  -out dnac.csr

dnac-example.key:
	@echo MAKE DNAC KEY
	openssl genrsa -des3 -out dnac-example.key 2048


# the private CA certificate and key
#
# these are held by the University authority
# (the creation here is just an example)

root.crt: root.csr
	@echo MAKE ROOT CRT
	openssl x509 -req -days 10982 -sha384 -in root.csr -signkey root.key \
	  -out root.crt -extfile root.cnf -extensions req_ext

root.csr: root.key root.cnf
	@echo MAKE ROOT CSR
	openssl req -new -config root.cnf -key root.key -out root.csr

root.key:
	@echo MAKE ROOT CA KEY
	openssl genrsa -des3 -out root.key 2048
